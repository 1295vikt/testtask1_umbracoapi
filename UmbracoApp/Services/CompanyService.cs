﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using UmbracoApp.Requests;
using UmbracoApp.Responses;

namespace UmbracoApp.Services
{
    public interface ICompanyService
    {
        IEnumerable<CompanyResponse> GetAll();
        CompanyResponse GetById(int id);
        CompanyResponse Create(CompanyRequest request);
        CompanyResponse Update(int id, CompanyRequest request);
        void Delete(int id);
    }

    public class CompanyService : ICompanyService
    {
        private readonly IUmbracoContextFactory _context;
        private readonly IContentService _contentService;
        private readonly IMapper _mapper;
        public CompanyService(IMapper mapper, IUmbracoContextFactory context, IContentService contentService)
        {
            _contentService = contentService;
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<CompanyResponse> GetAll()
        {
            using (var cref = _context.EnsureUmbracoContext())
            {
                var root = cref.UmbracoContext.Content.GetAtRoot().First();

                var content = root.Children.Where(x=>x.IsDocumentType("companyDetails"));

                if (content == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                var response = _mapper.Map<IEnumerable<CompanyResponse>>(content);

                return response;
            }
        }


        public CompanyResponse GetById(int id)
        {
            using (var cref = _context.EnsureUmbracoContext())
            {
                var root = cref.UmbracoContext.Content.GetAtRoot().First();

                var content = root.Children.Where(x => x.IsDocumentType("companyDetails"))
                    .FirstOrDefault(x => x.Value<int>("companyId") == id);

                if (content == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                var response = _mapper.Map<CompanyResponse>(content);

                return response;
            }
        }


        public CompanyResponse Create(CompanyRequest request)
        {
            using (var cref = _context.EnsureUmbracoContext())
            {
                var root = cref.UmbracoContext.Content.GetAtRoot().First();

                if (root.Children.Where(x => x.IsDocumentType("companyDetails")).Any(x => x.Name == request.CompanyName
                || x.Value<string>("contactPhone") == request.ContactPhone || x.Value<string>("contactEmail")==request.ContactEmail))
                {
                    throw new HttpResponseException(HttpStatusCode.Conflict);
                }

                var content = _contentService.Create(request.CompanyName, root.Id, "companyDetails");

                int id = 1;
                while (root.Children.Any(x => x.Value<int>("companyId") == id))
                {
                    id++;
                }

                content.SetValue("companyId", id);
                SetCompanyProperties(request, ref content);

                var publishedResult = _contentService.SaveAndPublish(content);

                if (publishedResult.Success)
                {
                    var publishedContent = cref.UmbracoContext.Content.GetById(publishedResult.Content.Id);
                    var response = _mapper.Map<CompanyResponse>(publishedContent);
                    return response;
                }

                throw new HttpResponseException(HttpStatusCode.InternalServerError);

            }
        }


        public CompanyResponse Update(int id, CompanyRequest request)
        {
            using (var cref = _context.EnsureUmbracoContext())
            {
                var root = cref.UmbracoContext.Content.GetAtRoot().First();

                var published = root.Children.Where(x => x.IsDocumentType("companyDetails")).FirstOrDefault(x => x.Value<int>("companyId") == id);

                if (published == null)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                if (root.Children.Any(x => (request.CompanyName != published.Name && x.Name == request.CompanyName)
                || (published.Value<string>("contactPhone") != request.ContactPhone && x.Value<string>("contactPhone") == request.ContactPhone)
                || (published.Value<string>("contactEmail") != request.ContactEmail && x.Value<string>("contactEmail") == request.ContactEmail)))
                {
                    throw new HttpResponseException(HttpStatusCode.Conflict);
                }

                var content = _contentService.GetById(published.Id);

                content.Name = request.CompanyName;
                SetCompanyProperties(request, ref content);


                var publishedResult = _contentService.SaveAndPublish(content);

                if (publishedResult.Success)
                {
                    var publishedContent = cref.UmbracoContext.Content.GetById(publishedResult.Content.Id);
                    var response = _mapper.Map<CompanyResponse>(publishedContent);
                    return response;
                }

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        public void Delete(int id)
        {
            using (var cref = _context.EnsureUmbracoContext())
            {
                var root = cref.UmbracoContext.Content.GetAtRoot().First();

                var publishedContent = root.Children.Where(x => x.IsDocumentType("companyDetails")).FirstOrDefault(x => x.Value<int>("companyId") == id);

                if (publishedContent == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                var content = _contentService.GetById(publishedContent.Id);

                _contentService.Delete(content);
            }
        }


        private void SetCompanyProperties(CompanyRequest request, ref IContent content)
        {
            content.SetValue("companyCountry", request.Country);
            content.SetValue("companyAddress", request.Address);
            content.SetValue("contactPhone", request.ContactPhone);
            content.SetValue("contactEmail", request.ContactEmail);
            content.SetValue("comment", request.Comment);
            content.SetValue("logoImage", request.LogoImageURL);
        }

    }
}