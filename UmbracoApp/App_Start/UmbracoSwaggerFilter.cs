﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace UmbracoApp.App_Start
{
    public class UmbracoSwaggerFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            foreach (var apiDescription in apiExplorer.ApiDescriptions)
            {
                var backofficeRoutes = swaggerDoc.paths
                    .Where(x => x.Key.Contains("BackOffice") || x.Key.Contains("backoffice") || x.Key.Contains("install")||x.Key.Contains("KeepAlive"));

                swaggerDoc.paths = swaggerDoc.paths.Except(backofficeRoutes).ToDictionary(x => x.Key, x => x.Value);
            }
        }
    }

}