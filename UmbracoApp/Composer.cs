﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Umbraco.Core.Composing;
using UmbracoApp.Services;

namespace UmbracoApp
{
    public class Composer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register(GetMapper, Lifetime.Singleton);
            composition.Register(typeof(ICompanyService), typeof(CompanyService), Lifetime.Transient);

            GlobalConfiguration.Configuration.MapHttpAttributeRoutes();
        }

        public IMapper GetMapper(IFactory factory)
        {
            var config = new MapperConfiguration(cfg =>
            {
                var profiles = GetType().Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(new AutomapperProfile());
                }
            });
            return new Mapper(config);
        }
    }

}