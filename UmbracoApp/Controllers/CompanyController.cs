﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedModels;
using Umbraco.Web.WebApi;
using UmbracoApp.Requests;
using UmbracoApp.Responses;
using UmbracoApp.Services;

namespace UmbracoApp.Controllers
{
    [RoutePrefix("api/companies")]
    public class CompanyController : UmbracoApiController
    {

        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<CompanyResponse> Get()
        {
            return _companyService.GetAll();
        }

        [HttpGet]
        [Route("{id}")]
        public CompanyResponse Get(int id)
        {
            return _companyService.GetById(id);
        }

        [HttpPost]
        [Route("")]
        public CompanyResponse Create([FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            return _companyService.Create(request);
        }

        [HttpPut]
        [Route("{id}")]
        public CompanyResponse Update(int id, [FromBody] CompanyRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            return _companyService.Update(id, request);
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            _companyService.Delete(id);
        }


    }
}
