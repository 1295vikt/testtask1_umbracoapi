﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.WebApi;

namespace UmbracoApp.Controllers
{
    [RoutePrefix("api/colors")]
    public class ColorController : UmbracoApiController
    {

        [HttpGet]
        [Route("")]
        public Dictionary<int,string> Get()
        {

            var dataType = Services.DataTypeService.GetDataType("Dropdown - Color Dictionary");      

            if (dataType != null)
            {
                ValueListConfiguration valueList = (ValueListConfiguration)dataType.Configuration;

                if (valueList != null && valueList.Items != null && valueList.Items.Any())
                {
                    var colorsDictionary = valueList.Items.ToDictionary(x => x.Id, x => x.Value);
                    return colorsDictionary;
                }
            }

            throw new HttpResponseException(HttpStatusCode.InternalServerError);
        }

        [HttpGet]
        [Route("{id}")]
        public string Get(int id)
        {

            var dataType = Services.DataTypeService.GetDataType("Dropdown - Color Dictionary");

            if (dataType != null)
            {
                ValueListConfiguration valueList = (ValueListConfiguration)dataType.Configuration;

                if (valueList != null && valueList.Items != null && valueList.Items.Any())
                {
                    var color = valueList.Items.FirstOrDefault(x => x.Id == id);
                    if (color == null)
                    {
                        throw new HttpResponseException(HttpStatusCode.NotFound);
                    }
                    return color.Value;
                }
            }

            throw new HttpResponseException(HttpStatusCode.InternalServerError);
        }
    }
}
