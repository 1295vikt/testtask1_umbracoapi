﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using UmbracoApp.Requests;
using UmbracoApp.Responses;


namespace UmbracoApp
{
    public class AutomapperProfile : Profile
    {

        public AutomapperProfile()
        {
            
            CreateMap<IPublishedContent, CompanyResponse>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .AfterMap((src, dest) =>
                {
                    dest.Id = src.Value<int>("companyId");
                    dest.CompanyName = src.Name.ToString();
                    dest.Country = src.Value<string>("companyCountry");
                    dest.Address = src.Value<string>("companyAddress");
                    dest.ContactPhone = src.Value<string>("contactPhone");
                    dest.ContactEmail = src.Value<string>("contactEmail");
                    dest.Comment = src.Value<string>("comment");
                    dest.LogoImageURL = src.Value<string>("logoImage");
                });          

        }

    }

}