﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UmbracoApp.Requests
{
    public class CompanyRequest
    {
        [Required]
        [RegularExpression(@"^[ A-Za-z]{2,30}$")]
        public string CompanyName { get; set; }
        [Required]
        [RegularExpression(@"^[ A-Za-z]{3,30}$")]
        public string Country { get; set; }
        [Required]
        [StringLength(250)]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})$")]
        public string ContactPhone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        [RegularExpression("^[A-Za-z0-9_\\+-]+(\\.[A-Za-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$")]
        public string ContactEmail { get; set; }
        [StringLength(500)]
        public string Comment { get; set; }
        [StringLength(250)]
        public string LogoImageURL { get; set; }
    }
}